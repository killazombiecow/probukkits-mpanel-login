<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Admin Panel</title>

		<link rel="stylesheet" href="assets/css/main.css">
	</head>
	<body>
		<div class="w">
			<h2>MPanel</h2>
			<form method="POST">
				<input type="text" id="username" name="username" placeholder="Username">
				<input type="password" id="password" name="password" placeholder="Password">
				<input type="submit" value="Login">
			</form>
		</div>
	</body>
</html>